import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "../views/Home.vue";
import BasketView from "../views/Basket.vue";

const routes = [
  {
    path: "/",
    name: "HomeView",
    component: HomeView,
  },
  {
    path: "/basket",
    name: "BasketView",
    component: BasketView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
